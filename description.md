# CrossEngage Python developer Challenge

## The Challenge

You have to develop a system that:

* Supports command-line flags to set options for everything that can be configurable.
* Processes large files of integer numbers and be able to sort those files (see example below)
* Publishes an HTTP API 
    * Where the user can submit a huge file for processing
    * Where the status of all possible files being sorted are returned
    * Where the user gets back the results of the sorted file

## Example
User could post files like
```
2
1000
1
...
```

potentially with billions of records.

The desired output is:
```
1
2
1000
```
i.e., a numerically sorted file with the original numbers.

A lexicographically sorted file, like
```
1
1000
2
```
will be considered wrong.

## General Information

* You can use any framework of your choice;
* Create a *private* Git repository (Bitbucket*, Github or Gitlab - your choice!) for you and invite `ashishbagri7` and `jaul` to have access to it;
* We will check your commits to see your progress in the project.
* The system must be able to run on Mac OS X, or Ubuntu.
* Give us instructions on how to use the system (build, run tests, deploy, run, etc). The process should be simple, if not, use something like Vagrant, because it will help you and us.
* You can use any external components (databases, caches, etc) if you wish.
* You don't need to worry about frontend. Just basic HTML markup in this page is enough.
* The project will be assessed for simplicity, clarity, architecture, documentation, coding style, automated tests, algorithm choice, and performance.
* Please, let the recruiter know when you have finished. Make sure to hand in your results by the agreed deadline.

*Bitbucket allows you to create a private repository on free accounts