import os
import subprocess

from flask import Flask, request, redirect, url_for, send_from_directory, render_template
from werkzeug.utils import secure_filename

from service.mergesort import sort

app = Flask(__name__, template_folder=os.path.abspath(os.path.dirname(__file__) + '/templates/'))

app.config['ROOT_PATH'] = os.path.abspath(os.path.dirname(__file__) + '/../')
app.config['UPLOAD_FOLDER'] = app.config['ROOT_PATH'] + '/tmp/'
app.config['SORTED_FILES_FOLDER'] = app.config['ROOT_PATH'] + '/sorted_files/'
app.config['ALLOWED_EXTENSIONS'] = ('txt', 'in')
app.config['DEBUG'] = True


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


@app.route('/')
def index():
    """Index (main) page handler

    :return: index page html
    :rtype: str
    """
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload():
    """
    Handle file uploading

    :return: call redirect to uploaded file page
    """
    file = request.files['file']

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)

        # TODO: 7GB file uploading is a problem. Need additional research
        file.save(os.path.abspath(os.path.join(app.config['UPLOAD_FOLDER'], filename)))

        with open(app.config['SORTED_FILES_FOLDER']+'sorted_'+filename, 'w+') as out:
            out.writelines('\n'.join(map(str, sort(app.config['UPLOAD_FOLDER']+filename))))
            out.close()

        return redirect(url_for('sorted_file', filename=filename))


@app.route('/sorted/<filename>')
def sorted_file(filename):
    """
    Handle downloading of sorted file. If file is not sorted the html page should be returned with status

    :param filename:
    :return:
    """

    return send_from_directory(app.config['SORTED_FILES_FOLDER'], 'sorted_' + filename)

    # if __name__ == "__main__":
    #     app.run(host='127.0.0.1', port=5000, debug=app.config['DEBUG'])
