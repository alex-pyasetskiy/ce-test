# TODO: GENERAL! Apply EAFP
import argparse


# TODO: Docstrings
def _merge(left, right):
    result, i, j = [], 0, 0
    while i < len(left) and j < len(right):
        if left[i] <= right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
    result += left[i:]
    result += right[j:]
    return result


# TODO: Docstrings
def _merge_sort(l):
    length = len(l)
    cut = length / 2
    if length <= 1:
        return l
    return _merge(_merge_sort(l[:int(cut)]), _merge_sort(l[int(cut):]))


# TODO: Docstrings
def sort(data):
    """
    Process merge sorting

    :param data: data to be sorted

    :return: sorted data
    """
    with open(data, 'r') as input_file:
        return _merge_sort([int(i.strip('\n')) for i in input_file.readlines()])


# TODO: Apply Command design pattern
# TODO: Encapsulate in 'libsort' package
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate an input file of random int`s.')
    parser.add_argument('f', metavar='INPUT', help='Output file destination (Default: ../tmp/generated.in)')
    parser.add_argument('-o', metavar='OUTPUT', help='Output file destination (Default: ../sorted_files/sorted.in)',
                        default='../sorted_files/sorted.out')
    args = parser.parse_args()

    with open(args.o, 'w+') as out:
        out.writelines('\n'.join(map(str, sort(args.f))))
        out.close()
