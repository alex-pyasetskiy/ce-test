"""
Utility script helps to generate large files of random integer numbers.
"""
import argparse
import random
import time

DEFAULT_BUFFER_SIZE = 1024000  # 100 Kb
# TODO: run in parallel 1000000000
DEFAULT_NUM_LIMIT = 1000000
DEFAULT_FILE_NAME = 'test.in'


def _generate(output, buffer=DEFAULT_BUFFER_SIZE, limit=DEFAULT_NUM_LIMIT):
    print("Generating '{filename} with {limit} integers...".format(filename=output, limit=limit))
    start_time = time.time()
    with open(output, 'w', buffering=buffer) as fo:
        for step in range(limit):
            if step % 10000 == 0:
                print("%s/%s" % (step, limit))
            fo.write('%s\n' % random.randrange(1, 32000))
    end_time = time.time()
    time_spent = (end_time - start_time)
    print("Complete !\n Time spent: {ts} sec".format(ts=time_spent))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate an input file of random int`s.')
    parser.add_argument('-f', metavar='OUTPUT', help='Output file destination (Default: tmp/generated.in)',
                        default=DEFAULT_FILE_NAME)
    parser.add_argument('-b', help='Buffer size (defaults to {0})'.format(DEFAULT_BUFFER_SIZE),
                        metavar='BYTES', default=DEFAULT_BUFFER_SIZE)
    parser.add_argument('-n', help='Number of elements (defaults to {0})'.format(DEFAULT_NUM_LIMIT),
                        default=DEFAULT_NUM_LIMIT)
    args = parser.parse_args()
    _generate(args.f, int(args.b), int(args.n))
